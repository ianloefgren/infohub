#!/usr/bin/env python

"""
Driver script for information hub.
"""

import os
import sys
import logging
import datetime

from time import localtime, strftime, sleep
from PIL import Image, ImageDraw, ImageFont
from waveshare_epd import epd2in7

import weather
import calendar_backend

UPDATE_PERIOD = 60 # seconds
WEATHER_UPDATE_PERIOD = 60 # seconds, 10 minutes
CALENDAR_UPDATE_PERIOD = 60 # seconds, 10 minutes

picdir = os.path.join(os.path.abspath('/home/pi/e-Paper/RaspberryPi&JetsonNano/python/'), 'pic')

font10 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 10)
font12 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 12)
font14 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 14)
font18 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 18)
font24 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 24)
font28 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 28)
font35 = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 35)

GRAY1  = 0xff #white
GRAY2  = 0xC0
GRAY3  = 0x80 #gray
GRAY4  = 0x00 #Blackest

def get_time(format_str='%a,%b,%d,%I,%M,%p'):
    return strftime(format_str, localtime()).split(',')

def draw_clock(draw_obj,time_array):
    # draw date
    draw_obj.text((185,120),'{}, {} {}'.format(time_array[0],time_array[1],time_array[2]),font=font14,fill=GRAY4)
    # draw time
    draw_obj.text((150,135),'{}:{}{}'.format(time_array[3],time_array[4],time_array[5].lower()),font=font28,fill=GRAY4)
    return draw_obj

def draw_weather(draw_obj,weather,weather_icon_path):
    # draw temperature
    draw_obj.text((10,135),'{} {}'.format(weather[0]['temperature'],weather[0]['temperatureUnit']),font=font28,fill=GRAY4)
    # load icon
    with Image.open(weather_icon_path) as icon_bmp:
        # draw icon
        draw_obj.bitmap((65,140),icon_bmp,fill=GRAY4)

    return draw_obj

def draw_picture(draw_obj,image_path):
    # draw picture that takes whole screen
    with Image.open(image_path).convert('RGBA') as image_bmp:
        draw_obj.bitmap((0,0),image_bmp,fill=GRAY4)

    return draw_obj

def draw_calendar(draw_obj,events,num_events=2):
    # make sure there are events
    if events is not None:
        # make sure there aren't too many events (we only draw next 2)
        if len(events) > num_events:
            events = events[0:num_events]

        # get times and summaries
        primary_event_start = events[0]['start'].strftime('%a,%b,%d,%I,%M,%p').split(',')
        primary_event_end = events[0]['end'].strftime('%a,%b,%d,%I,%M,%p').split(',')
        primary_event_summary = events[0]['summary']

        # primary event description
        draw_obj.text((110,10),primary_event_summary,font=font24,fill=GRAY4)
        # primary event time, just below event description
        draw_obj.text((120,35),'{}:{}{}-{}:{}{}'.format(primary_event_start[3],primary_event_start[4],primary_event_start[5].lower(),
                    primary_event_end[3],primary_event_end[4],primary_event_end[5].lower()),font=font14,fill=GRAY4)
        
        if len(events) > 1:
            second_event_start = events[1]['start'].strftime('%a,%b,%d,%I,%M,%p').split(',')
            second_event_end = events[1]['end'].strftime('%a,%b,%d,%I,%M,%p').split(',')
            second_event_summary = events[1]['summary']

            # second event description
            draw_obj.text((140,60),second_event_summary,font=font14,fill=GRAY4)
            # second event time, just below event description
            draw_obj.text((160,75),'{}:{}{}-{}:{}{}'.format(second_event_start[3],second_event_start[4],second_event_start[5].lower(),
                        second_event_end[3],second_event_end[4],second_event_end[5].lower()),font=font10,fill=GRAY4)

    else:
        # if no events, just add some text saying so
        draw_obj.text((130,50),'No upcoming events.',font=font24,fill=GRAY4)
    
    return draw_obj


# def update_screen(screen_obj,weather_obj,run_time)

def main():
    # create e-paper display instance
    epd = epd2in7.EPD()
    epd.Init_4Gray()
    epd.Clear(0xFF) # clears screen to white

    # create weather backend instance
    w = weather.Weather()

    # create calendar backend instance
    cal = calendar_backend.CalendarBackend()

    # create reminder instance

    # update every `update rate` seconds
    run_time = 0 # run time in seconds
    old_t_array = [None,None,None,None,None,None]
    try:
        while True:
            # reset update flag
            update_flag = False

            # create new image to draw on
            Limage = Image.new('L', (epd.height, epd.width), 255)  # 255: clear the frame
            draw = ImageDraw.Draw(Limage)

            # update clock
            t_array = get_time()
            if any([x != y for (x,y) in zip(old_t_array,t_array)]):
                update_flag = True
                draw = draw_clock(draw,t_array)
            old_t_array = t_array

            # update weather
            if run_time % WEATHER_UPDATE_PERIOD == 0:
                update_flag = True
                weather_dict = w.get_weather()
                weather_icon_path = w.get_weather_icon(weather_dict[0]['shortForecast'])
                draw = draw_weather(draw,weather_dict,weather_icon_path)

            # update alarm

            # get new events for calendar
            if run_time % CALENDAR_UPDATE_PERIOD == 0:
                update_flag = True
                events = cal.get_events()
                draw = draw_calendar(draw,events)

            # update reminders

            # update status symbols

            # have to flip the image for some reason
            Limage = Limage.transpose(Image.FLIP_LEFT_RIGHT)

            if update_flag:
                epd.display_4Gray(epd.getbuffer_4Gray(Limage))

            # sleep until next update
            sleep(UPDATE_PERIOD)
            run_time += UPDATE_PERIOD

    except KeyboardInterrupt as e:
        # epd.Clear(0xFF)
        logging.info('Shutting down...')
        epd2in7.epdconfig.module_exit()
        exit()

def test_layout(image_path):
    """
    Test layout using full screen image.
    """
    # create e-paper display instance
    epd = epd2in7.EPD()
    epd.Init_4Gray()
    epd.Clear(0xFF) # clears screen to white

    # create new image to draw on
    Limage = Image.new('L', (epd.height, epd.width), 255)  # 255: clear the frame
    draw = ImageDraw.Draw(Limage)

    draw = draw_picture(draw, image_path)

    # have to flip the image for some reason
    Limage = Limage.transpose(Image.FLIP_LEFT_RIGHT)

    epd.display_4Gray(epd.getbuffer_4Gray(Limage))

    logging.info('Shutting down...')
    epd2in7.epdconfig.module_exit()
    exit()

if __name__ == "__main__":
    print(len(sys.argv))
    if len(sys.argv) == 1:
        main()
    elif len(sys.argv) == 3 and sys.argv[1] == 'test':
        image_path = os.path.abspath(sys.argv[2])
        test_layout(image_path)
