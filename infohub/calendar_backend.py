#!/usr/bin/env python

"""
Calendar backend for infohub.
"""

import datetime
import pickle
import os
import pprint
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']

class CalendarBackend:
    """
    Backend for information hub calendar. Uses the Google Calendar API
    to access events.
    """
    def __init__(self,token_path='../resources/calendar/token.pickle',
                cred_path='../resources/calendar/credentials.json.secrets'):

        # get user credentials
        self.creds = self.get_credentials(os.path.abspath(token_path),os.path.abspath(cred_path))
        # build calendar service
        self.service = build('calendar', 'v3', credentials=self.creds,cache_discovery=False)


    def get_credentials(self,token_path,cred_path):
        """
        Get credentials for user. Loads saved token if it exists, and creates
        a new one from the credentials json file if not.

        This is pulled straight from the basic guide on the Google
        Calendar API site.
        """
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(token_path):
            with open(token_path, 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    cred_path, SCOPES)
                # creds = flow.run_local_server(port=0)
                creds = flow.run_console()
            # Save the credentials for the next run
            with open(token_path, 'wb') as token:
                pickle.dump(creds, token)

        return creds

    def get_events(self,num_events=2,calendars='primary',start_time=None,end_time=None):
        """
        Get the specified number of events from the specified calendars.

        Parameters
        ----------
        num_events : default 2
            maximum number of events to return
        calendars : default 'primary'
            names of calendars from which to grab events, NOTE: currently only supports 'primary'
        start_time
            start time for search period
        end_time
            end time for search period

        Returns
        -------
        events
            list of events found, with each element as a dict of 
                ('start': start_time,'end': end_time,'summary': summary)
            returns None if no events found
        """
        # Call the Calendar API
        now = datetime.datetime.utcnow()
        now_iso = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        future = datetime.timedelta(days=1)
        maxtime = (now+future).isoformat() + 'Z'

        # if calendars == 'all':
        #     calendar_list = self.service.calendarList().list().execute()
        #     # pprint.pprint(calendar_list)
        #     for calendar_list_entry in calendar_list['items']:
        #         print(calendar_list_entry['summary'])

        events_result = self.service.events().list(calendarId='primary', timeMin=now_iso,
                                            timeMax=maxtime,maxResults=num_events, singleEvents=True,
                                            orderBy='startTime').execute()
        events = events_result.get('items', [])

        events_list = []

        if not events:
            return None

        for event in events:
            # get start time, end time, parse to datetime objects
            start_str = event['start'].get('dateTime', event['start'].get('date'))
            start_str = ''.join(start_str.rsplit(':',1))
            start = datetime.datetime.strptime(start_str,'%Y-%m-%dT%H:%M:%S%z')
            end_str = event['end'].get('dateTime', event['end'].get('date'))
            end_str = ''.join(end_str.rsplit(':',1))
            end = datetime.datetime.strptime(end_str,'%Y-%m-%dT%H:%M:%S%z')

            # get summary
            summary = event['summary']

            # we assume the google calendar will return event times in device local time
            start_local = start
            end_local = end

            events_list.append({'start': start_local,'end': end_local,'summary': summary})

            # print(start, event['summary'])

        return events_list

if __name__ == "__main__":
    c = CalendarBackend()
    print(c.get_events())