#!/usr/bin/env python

import os
import time
import sys
import json
import requests
import pprint
import yaml

class Weather:
    """
    Class to manage getting weather and forecast information from the
    National Weather Service API.
    """
    def __init__(self,icon_dir='../resources/weather_icons/png/'):
        # load location information from file
        loc_info = self.load_location_file()
        self.lat = loc_info['LAT']
        self.lon = loc_info['LONG']
        self.gridx = loc_info['GRIDX']
        self.gridy = loc_info['GRIDY']
        self.office = loc_info['OFFICE']

        self.icon_dir = os.path.abspath(icon_dir)

    def load_location_file(self,fn='../resources/weather_location.secrets'):
        """
        Load location data from 'secret' file, so location isn't on github...
        Assumes a yaml file is used to contain this information.

        Parameters
        ----------
        fn
            path to filename, default '../resources/weather.secrets'

        Returns
        -------
        location_dict
            dictionary containing location info
            {LAT,LONG,GRIDX,GRIDY,OFFICE}
            location latitude, location longitude, NWS grid x coord, grid y coord, NWS forecast office
        """
        with open(os.path.abspath(fn),'r') as f:
            weather_location_dict = yaml.load(f,Loader=yaml.SafeLoader)
        return weather_location_dict

    def get_grid_coords(self,lat,lon,save=False,fn='../resources/weather_location.secrets'):
        """
        Get National Weather Service grid coordinates from passed lat long.
        Saves data to 'secrets' file if desired.

        Parameters
        ----------
        lat
            location latitude
        lon
            location longitude
        save: default False
            save location information to file
        fn: default '../resources/weather_location.secrets'
            filename for secrets file

        Returns
        -------
        location_dict
            dictionary of location information
            {LAT,LONG,GRIDX,GRIDY,OFFICE}
            location latitude, location longitude, NWS grid x coord, grid y coord, NWS forecast office
        """
        url = 'https://api.weather.gov/points/{},{}'.format(lat,lon)
        r = requests.get(url)
        data = r.json()
        location_dict = {'LAT': lat,
                        'LONG': lon,
                        'GRIDX': data['properties']['gridX'],
                        'GRIDY': data['properties']['gridY'],
                        'OFFICE': data['properties']['cwa']}

        # if save, write to file
        if save:
            with open(os.path.abspath(fn),'w') as f:
                yaml.dump(location_dict,f,Dumper=yaml.SafeDumper,default_flow_style=False)

        return location_dict

    def get_weather(self,num_hours=1,fields=['temperature','temperatureUnit','shortForecast']):
        """
        Get weather from the National Weather Service API.
        """
        url = 'https://api.weather.gov/gridpoints/{}/{},{}/forecast/hourly'.format(self.office,self.gridx,self.gridy)
        r = requests.get(url)
        data = r.json()['properties']['periods'][0:num_hours]
        return_data = []
        for hour in data:
            hour_dict = {}
            for field in fields:
                hour_dict[field] = hour[field]
            return_data.append(hour_dict)
        return return_data

    def get_weather_icon(self,weather_str):
        """
        Get location of appropriate weather icon given test description.

        Parameters
        ----------
        weather_str
            string with description of weather. Used to map to appropriate icon filename
        
        Returns
        -------
            full path to appropriate icon, using icon directory location specified on init
        """
        fn = icon_mapping[weather_str]
        return os.path.join(self.icon_dir,fn)

icon_mapping = {
    'Sunny': 'wi-day-sunny_30px.png',
    'Mostly Sunny': 'wi-day-sunny-overcast_30px.png',
    'Partly Sunny': 'wi-day-cloudy_30px.png',
    'Partly Cloudy': 'wi-day-cloudy_30px.png',
    'Mostly Cloudy': 'wi-day-cloudy-high_30px.png',
    'Cloudy': 'wi-cloudy_30px.png'
}

if __name__ == "__main__":
    # get_grid_coords(LAT,LONG)
    # get_weather(4)
    w = Weather()
    print(w.get_weather(1))
    print(w.get_weather_icon(w.get_weather(1)[0]['shortForecast']))
    # w.get_grid_coords(w.lat,w.lon,save=True)